from flask import render_template

from spcmetrics import app


@app.route('/')
@app.route('/index')
def index():
    return render_template("coming_soon.html")
